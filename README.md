# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone git@bitbucket.org:aljo13/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/aljo13/stroboskop/commits/98e803a50bfb63613ea58c580ef8ee8c1f9362ff

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/aljo13/stroboskop/commits/1ea3729ca8ebc5a93e433581277fc99788b66027

Naloga 6.3.2:
https://bitbucket.org/aljo13/stroboskop/commits/cad819d85aef951d223e1182fae38a3c333dd581

Naloga 6.3.3:
https://bitbucket.org/aljo13/stroboskop/commits/e946962bee4461c77f5fa02a08a15d67c4b84586

Naloga 6.3.4:
https://bitbucket.org/aljo13/stroboskop/commits/00ac61512ec2175b0b053a15bd265c96fee26dc0

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/aljo13/stroboskop/commits/f1d0467d17f68891c2f26906296d5d3641fe1a44

Naloga 6.4.2:
https://bitbucket.org/aljo13/stroboskop/commits/0fc22ca9766f8a07298fc0939e3c83ef20028c29

Naloga 6.4.3:
https://bitbucket.org/aljo13/stroboskop/commits/8499e9d974d664e4c4a3db124e49c83050872a98

Naloga 6.4.4:
https://bitbucket.org/aljo13/stroboskop/commits/258f1f9e1721212af7770fd36fdb1604920a3041